package achozen.rememberme.firebase.statistics;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

import achozen.rememberme.firebase.statistics.model.Score;

/**
 * Created by piad on 2017-04-02.
 * <p>
 * This class can be used to store Scores between games or single levels
 */

public class GameScoreCounter {
    private static GameScoreCounter instance = new GameScoreCounter();
    private static Map<String, Integer> activeGameScoreCounter = new HashMap<>();

    public static GameScoreCounter getInstance() {
        return instance;
    }

    public void setScore(String gameId, int score) {
        activeGameScoreCounter.put(gameId, score);
    }

    public Score getScore(String gameId) {
        Score score = new Score();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            score.email = user.getEmail();
            score.name = user.getDisplayName();
        }
        Integer scoreNumber = activeGameScoreCounter.get(gameId);
        if (scoreNumber == null) {
            score.score = 0;
        } else {
            score.score = activeGameScoreCounter.get(gameId);
        }
        return score;
    }

    public void clearAllScores() {
        activeGameScoreCounter.clear();
    }

    public void clearScore(String gameId) {
        activeGameScoreCounter.remove(gameId);
    }
}
