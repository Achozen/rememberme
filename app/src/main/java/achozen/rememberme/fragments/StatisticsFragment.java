package achozen.rememberme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import achozen.rememberme.R;
import achozen.rememberme.statistics.GameStatistics;

/**
 * Created by Achozen on 2016-02-27.
 */
public class StatisticsFragment extends Fragment {
    private GameStatistics gameStatistics;

    public void setStatistics(GameStatistics gameStatistics) {
        this.gameStatistics = gameStatistics;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_statistics, container, false);

        TextView gameState = (TextView) view.findViewById(R.id.gameStateTextView);
        TextView gameScore = (TextView) view.findViewById(R.id.gameScoreTextView);
        TextView timeSpent = (TextView) view.findViewById(R.id.timeSpentTextView);
        if (gameStatistics != null) {
            gameState.setText(gameStatistics.getGameState().toString());
            gameScore.setText(String.valueOf(gameStatistics.getScoredPoints()));
            timeSpent.setText(String.valueOf(gameStatistics.getGameTime()));
        }

        return view;
    }
}
