package achozen.rememberme.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import achozen.rememberme.R;
import achozen.rememberme.adapter.HighScoreAdapter;
import achozen.rememberme.firebase.statistics.model.Score;

import static achozen.rememberme.activities.HighScoresActivity.MY_RANING;
import static achozen.rememberme.activities.HighScoresActivity.MY_RANKING;
import static achozen.rememberme.activities.HighScoresActivity.MY_SCORES;
import static achozen.rememberme.activities.HighScoresActivity.MY_SCORES_TEXT;
import static achozen.rememberme.activities.HighScoresActivity.SCORE_TYPE;
import static achozen.rememberme.activities.HighScoresActivity.TOP100;
import static achozen.rememberme.activities.HighScoresActivity.TOP_100_TEXT;

/**
 * Created by piad on 2017-04-01.
 */

public class FirebaseHighScoresFragment extends android.support.v4.app.Fragment implements ValueEventListener {
    public static final String HIGH_SCORES_DATABASE = "high_scores";
    private View rootView;
    private RecyclerView mRecyclerView;
    private HighScoreAdapter mAdapter;
    private List<Score> highScores = new ArrayList<>();
    private DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_high_scores, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.highScoresRecyclerView);
        createDBConnection();
        setupRecyclerView();


        Bundle args = getArguments();
        final int scoreType = args.getInt(SCORE_TYPE);
        switch (scoreType) {
            case TOP100:
                prepareTop100Fragment();
                break;
            case MY_RANKING:
                prepareMyRankingFragment(FirebaseHighScoresFragment.this);
                break;
            case MY_SCORES:
                prepareMyScoresFragment();
                break;
        }

        return rootView;
    }

    private void setupRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mAdapter = new HighScoreAdapter(new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void prepareTop100Fragment() {
        ((TextView) rootView.findViewById(R.id.score_header)).setText(TOP_100_TEXT);

        Query myTopPostsQuery = databaseReference.orderByChild("score").limitToLast(100);
        myTopPostsQuery.addListenerForSingleValueEvent(this);
    }

    private void prepareMyScoresFragment() {
        ((TextView) rootView.findViewById(R.id.score_header)).setText(MY_SCORES_TEXT);
    }

    private void prepareMyRankingFragment(FirebaseHighScoresFragment fragment) {
        ((TextView) rootView.findViewById(R.id.score_header)).setText(MY_RANING);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        String email = "";
        if (user != null) {
            email = user.getEmail();
        }
        Log.d("TAGTAG", "email " + email);
        Query myTopPostsQuery = databaseReference.orderByChild("email").equalTo(email);
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Score myScore = dataSnapshot.getValue(Score.class);
                Log.d("TAGTAG", " prepareMyRankingFragment onDataChange1: " + dataSnapshot.getValue());
                Log.d("TAGTAG", "onDataChange :score " + myScore);
                if (myScore == null) {
                    return;
                }
                Query myTopPostsQuery = databaseReference.orderByChild("score").startAt(myScore.score).limitToLast(5);
                myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d("TAGTAG", " prepareMyRankingFragment onDataChange2 " + dataSnapshot.getValue().toString());
                        highScores.clear();
                        for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                            Score score = messageSnapshot.getValue(Score.class);
                            highScores.add(score);
                        }

                        Collections.reverse(highScores);
                        mAdapter.swap(highScores);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createDBConnection() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(HIGH_SCORES_DATABASE);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        Log.d("TAGTAG", " onDataChange: " + dataSnapshot.getValue().toString());
        highScores.clear();
        for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
            Score score = messageSnapshot.getValue(Score.class);
            highScores.add(score);
        }

        Collections.reverse(highScores);
        mAdapter.swap(highScores);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}




