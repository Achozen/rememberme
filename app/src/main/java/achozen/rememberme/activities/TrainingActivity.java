package achozen.rememberme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import achozen.rememberme.R;
import achozen.rememberme.engine.GameInitializationData;
import achozen.rememberme.engine.GameProgressCoordinator;
import achozen.rememberme.engine.OnLevelFinishListener;
import achozen.rememberme.enums.GameMode;
import achozen.rememberme.fragments.PatternGameFragment;
import achozen.rememberme.interfaces.GameProgressListener;
import achozen.rememberme.navigation.FragmentNavigator;
import achozen.rememberme.statistics.GameState;
import achozen.rememberme.statistics.GameStatistics;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Achozen on 2016-02-23.
 */
public class TrainingActivity extends FragmentActivity implements GameProgressListener,
        OnLevelFinishListener, GoogleApiClient.OnConnectionFailedListener {

    private GameProgressCoordinator gameProgressCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        ButterKnife.bind(this);
        requestForAds();

        gameProgressCoordinator = new GameProgressCoordinator(this, this, GameMode.TRAINING);
        gameProgressCoordinator.startGame();
    }

    @OnClick(R.id.restartButton)
    void restartClickListener(View v) {
        gameProgressCoordinator.startNextLevel(null);
    }

    @OnClick(R.id.backButton)
    void backClickListener(View v) {
        Intent intent = new Intent(TrainingActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.settingsButton)
    void settingsClickListener(View v) {
        Intent intent = new Intent(TrainingActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void startNewLevel(GameInitializationData gameInitializationData) {
        PatternGameFragment gameFragment = new PatternGameFragment();
        gameFragment.setGameInitializationData(gameInitializationData);
        gameFragment.setOnLevelFinishListener(this);
        FragmentNavigator.navigateToNextFragment(TrainingActivity.this, gameFragment);
    }

    @Override
    public void onGameFinished(GameStatistics gameStatistics, String gameId) {
        finish();
    }

    private void requestForAds() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onLevelFinished(GameStatistics gameStatistics) {
        if (GameState.FAILED.equals(gameStatistics.getGameState())) {
            finish();
        } else {
            gameProgressCoordinator.startNextLevel(gameStatistics);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }
}
