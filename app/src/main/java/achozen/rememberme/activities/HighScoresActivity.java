package achozen.rememberme.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import achozen.rememberme.R;
import achozen.rememberme.fragments.FirebaseHighScoresFragment;

/**
 * Created by Achozen on 2016-02-27.
 */
public class HighScoresActivity extends FragmentActivity {

    public static final String TOP_100_TEXT = "TOP 100";
    public static final String MY_SCORES_TEXT = "MY SCORES";
    public static final String MY_RANING = "MY RANKING";
    public static final String SCORE_TYPE = "score_type";

    public static final int TOP100 = 0, MY_RANKING = 1, MY_SCORES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scoress);
        ScoresPagerAdapter mDemoCollectionPagerAdapter = new ScoresPagerAdapter(
                getSupportFragmentManager(), getApplicationContext());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public static class ScoresPagerAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 3;
        private Context context;

        public ScoresPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new FirebaseHighScoresFragment();
            Bundle args = new Bundle();
            args.putInt(SCORE_TYPE, i);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case TOP100:
                    return context.getString(R.string.top_100_tab_title);
                case MY_RANKING:
                    return context.getString(R.string.my_ranking_tab_title);
                case MY_SCORES:
                    return context.getString(R.string.my_scores_tab_title);
            }
            return "";
        }
    }


}
