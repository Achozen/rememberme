package achozen.rememberme.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import achozen.rememberme.R;
import achozen.rememberme.engine.GameInitializationData;
import achozen.rememberme.engine.GameProgressCoordinator;
import achozen.rememberme.engine.OnLevelFinishListener;
import achozen.rememberme.enums.GameMode;
import achozen.rememberme.firebase.statistics.GameScoreCounter;
import achozen.rememberme.fragments.PatternGameFragment;
import achozen.rememberme.fragments.StatisticsFragment;
import achozen.rememberme.interfaces.GameProgressListener;
import achozen.rememberme.navigation.FragmentNavigator;
import achozen.rememberme.statistics.GameState;
import achozen.rememberme.statistics.GameStatistics;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Achozen on 2016-02-23.
 */
public class GameActivity extends FragmentActivity implements GameProgressListener,
        OnLevelFinishListener {

    GameProgressCoordinator gameProgressCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        requestForAds();

        gameProgressCoordinator = new GameProgressCoordinator(this, this, GameMode.RANKING);
        gameProgressCoordinator.startGame();
    }

    @OnClick(R.id.restartButton)
    void restartClickListener(View v) {
        gameProgressCoordinator.startNextLevel(null);
    }

    @Override
    public void startNewLevel(GameInitializationData gameInitializationData) {
        PatternGameFragment gameFragment = new PatternGameFragment();
        gameFragment.setGameInitializationData(gameInitializationData);
        gameFragment.setOnLevelFinishListener(this);
        FragmentNavigator.navigateToNextFragment(GameActivity.this, gameFragment);
    }

    @Override
    public void onGameFinished(GameStatistics gameStatistics, String gameId) {
        GameScoreCounter.getInstance().clearScore(gameId);

        StatisticsFragment statisticsFragment = new StatisticsFragment();
        statisticsFragment.setStatistics(gameStatistics);
        FragmentNavigator.navigateToStatisticsFragment(this, statisticsFragment);
    }

    private void requestForAds() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onLevelFinished(GameStatistics gameStatistics) {
        incrementScoreValue(gameStatistics);
        if (GameState.FAILED.equals(gameStatistics.getGameState())) {
            finish();
        } else {
            gameProgressCoordinator.startNextLevel(gameStatistics);
        }
    }

    private void incrementScoreValue(GameStatistics gameStatistics) {
        int result = gameStatistics.getScoredPoints();
        result += GameScoreCounter.getInstance().getScore(gameProgressCoordinator.getActiveGameId()).score;
        GameScoreCounter.getInstance().setScore(gameProgressCoordinator.getActiveGameId(), result);

        Toast.makeText(this, "Stored score: " + GameScoreCounter.getInstance().getScore(gameProgressCoordinator.getActiveGameId()).score, Toast.LENGTH_LONG).show();
    }
}
