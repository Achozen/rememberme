package achozen.rememberme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import achozen.rememberme.R;
import achozen.rememberme.engine.PeferencesUtil;
import achozen.rememberme.enums.Difficulty;
import achozen.rememberme.enums.GameMode;
import achozen.rememberme.enums.GameSize;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static achozen.rememberme.activities.MenuActivity.GAME_MODE;
import static achozen.rememberme.engine.PeferencesUtil.Preferences.DIFFICULTY;
import static achozen.rememberme.engine.PeferencesUtil.Preferences.SIZE;
import static achozen.rememberme.enums.Difficulty.EASY;
import static achozen.rememberme.enums.Difficulty.HARD;
import static achozen.rememberme.enums.GameSize.BIG;
import static achozen.rememberme.enums.GameSize.SMALL;


/**
 * Created by Achozen on 2016-05-26.
 */
public class SettingsActivity extends Activity {

    @BindView(R.id.difficultyEasyButton)
    ImageView easyButton;
    @BindView(R.id.difficultyMediumButton)
    ImageView mediumButton;
    @BindView(R.id.difficultyHardButton)
    ImageView hardButton;
    @BindView(R.id.sizeSmallButton)
    ImageView smallSizeButton;
    @BindView(R.id.sizeMediumButton)
    ImageView mediumSizeButton;
    @BindView(R.id.sizeBigButton)
    ImageView bigSizeButton;
    @BindView(R.id.titleSizeLayout)
    View titleSizeLayout;
    @BindView(R.id.sizeLayout)
    View sizeLayout;
    @BindView(R.id.settingsTitle)
    TextView settingsTitle;

    private Context context;
    private GameMode gameMode;
    private GameSize pickedSize;
    private Difficulty pickedDifficulty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        context = this;
        gameMode = (GameMode) getIntent().getExtras().get(GAME_MODE);
        markButtonForCurrentSizeSettings();
        markButtonForCurrentDifficultySettings();
        hideGameSizeOnRanked();
        requestForAds();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void hideGameSizeOnRanked() {
        if (gameMode == GameMode.RANKING) {
            sizeLayout.setVisibility(View.GONE);
            titleSizeLayout.setVisibility(View.GONE);
            settingsTitle.setText(R.string.ranked_game_settings_title);
        }
    }

    @OnClick(R.id.difficultyEasyButton)
    void difficultyEasyButtonClickListener(View v) {
        clearDifficultyButtonsMarks();
        setPickedIcon(v, R.drawable.easy_button_picked);
        pickedDifficulty = EASY;
    }

    @OnClick(R.id.difficultyMediumButton)
    void difficultyMediumButtonClickListener(View v) {
        clearDifficultyButtonsMarks();
        setPickedIcon(v, R.drawable.medium_button_picked);
        pickedDifficulty = Difficulty.MEDIUM;
    }

    @OnClick(R.id.difficultyHardButton)
    void difficultyHardButtonClickListener(View v) {
        clearDifficultyButtonsMarks();
        setPickedIcon(v, R.drawable.hard_button_picked);
        pickedDifficulty = HARD;
    }

    @OnClick(R.id.sizeSmallButton)
    void sizeSmallButtonClickListener(View v) {
        clearSizeButtonsMarks();
        setPickedIcon(v, R.drawable.small_button_picked);
        pickedSize = SMALL;
    }

    @OnClick(R.id.sizeMediumButton)
    void sizeMediumButtonClickListener(View v) {
        clearSizeButtonsMarks();
        setPickedIcon(v, R.drawable.medium_size_button_picked);
        pickedSize = GameSize.MEDIUM;
    }

    @OnClick(R.id.sizeBigButton)
    void sizeBigButtonClickListener(View v) {
        clearSizeButtonsMarks();
        setPickedIcon(v, R.drawable.big_button_picked);
        pickedSize = BIG;
    }

    @OnClick(R.id.saveButton)
    void saveButtonlickListener(View v) {
        if (pickedSize != null) {
            PeferencesUtil.storeInPrefs(context, SIZE, pickedSize);
        }
        if (pickedDifficulty != null) {
            PeferencesUtil.storeInPrefs(context, DIFFICULTY, pickedDifficulty);
        }
        finish();
        Intent intent;
        if (gameMode == GameMode.TRAINING) {
            intent = new Intent(this, TrainingActivity.class);
        } else {
            intent = new Intent(this, GameActivity.class);
        }
        startActivity(intent);

    }

    private void setPickedIcon(View view, int id) {
        ((ImageView) view).setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, id));
    }

    private void clearSizeButtonsMarks() {
        smallSizeButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.small_button));
        mediumSizeButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.medium_size_button));
        bigSizeButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.big_button));
    }

    private void clearDifficultyButtonsMarks() {

        easyButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.easy_button));
        mediumButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.medium_button));
        hardButton.setImageDrawable(ContextCompat.getDrawable
                (SettingsActivity.this, R.drawable.hard_button));
    }

    private void markButtonForCurrentSizeSettings() {
        String currentSize = PeferencesUtil.readFromPrefs(context, SIZE);

        if (SMALL.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(smallSizeButton, R.drawable.small_button_picked);
        }
        if (GameSize.MEDIUM.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(mediumSizeButton, R.drawable.medium_size_button_picked);
        }
        if (BIG.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(bigSizeButton, R.drawable.big_button_picked);
        }
    }

    private void markButtonForCurrentDifficultySettings() {
        String currentSize = PeferencesUtil.readFromPrefs(context, DIFFICULTY);

        if (EASY.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(easyButton, R.drawable.easy_button_picked);
        }
        if (Difficulty.MEDIUM.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(mediumButton, R.drawable.medium_button_picked);
        }
        if (HARD.toString().equalsIgnoreCase(currentSize)) {
            setPickedIcon(hardButton, R.drawable.hard_button_picked);
        }
    }

    private void requestForAds() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
