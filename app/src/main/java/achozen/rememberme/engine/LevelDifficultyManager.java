package achozen.rememberme.engine;

import java.util.ArrayList;

import achozen.rememberme.enums.Difficulty;
import achozen.rememberme.enums.GameMode;
import achozen.rememberme.enums.GameSize;
import achozen.rememberme.interfaces.PointPosition;

import static achozen.rememberme.enums.GameMode.RANKING;
import static achozen.rememberme.enums.GameMode.TRAINING;
import static achozen.rememberme.enums.GameSize.BIG;
import static achozen.rememberme.enums.GameSize.MEDIUM;
import static achozen.rememberme.enums.GameSize.SMALL;

/**
 * Created by Achozen on 2016-06-01.
 */
class LevelDifficultyManager {
    private static final int MAX_LEVEL_FOR_TRAINING = 1;
    //Ranking values
    private static final int MAX_SMALL_LEVEL_FOR_RANKING = 1;
    private static final int MAX_MEDIUM_LEVEL_FOR_RANKING = 2;
    private static final int MAX_LEVEL_FOR_RANKING = 3;

    // There will be two modes of playing this game:
    // 1 - Training - user will be able to pick exact level and size e.g MEDIUM_BIG and play 5
    //     games on this lvl.
    // 2 - Ranking - user will be able to pick difficulty, and he will be forced to play through
    //     SMALL MEDIUM and HARD levels with the best time.
    // Each difficulty mode will be adjusting to be little harder each level;
    // Numbers of links for EASY mode
    //
    // MAX NUMBER OF LINKS FOR GAME SIZES ARE:
    // SMALL(3X3) - 9
    // MEDIUM(4X4) - 16
    // LARGE(5X5) - 25

    //Numbers of links for EASY mode
    private static final int[] EASY_SMALL = {3, 4, 4, 5, 5};
    private static final int[] EASY_MEDIUM = {5, 6, 6, 7, 7};
    private static final int[] EASY_BIG = {7, 8, 8, 8, 9};

    //Numbers of links for MEDIUM mode
    private static final int[] MEDIUM_SMALL = {4, 4, 5, 5, 6};
    private static final int[] MEDIUM_MEDIUM = {6, 7, 7, 8, 8};
    private static final int[] MEDIUM_BIG = {8, 9, 9, 10, 10};

    //Numbers of links for HARD mode
    private static final int[] HARD_SMALL = {7, 7, 8, 8, 9};
    private static final int[] HARD_MEDIUM = {10, 11, 12, 13, 14};
    private static final int[] HARD_BIG = {14, 15, 16, 17, 18};

    private int currentLevel;
    private int inLevelGameNumber;
    private GameMode gameMode;
    private Difficulty difficulty;
    private GameSize gameSize;

    /**
     * Use this constructor for creating training games
     */
    LevelDifficultyManager(Difficulty difficulty, GameSize gameSize) {
        gameMode = TRAINING;
        this.difficulty = difficulty;
        this.gameSize = gameSize;
    }

    /**
     * Use this constructor for creating ranked games
     */
    public LevelDifficultyManager(Difficulty difficulty) {
        gameMode = RANKING;
        this.difficulty = difficulty;
        gameSize = SMALL;
    }

    ArrayList<PointPosition> createPatternForNextLevel() {
        switch (gameMode) {
            case TRAINING:
                return createNextTrainingLevel();
            case RANKING:
                return createNextRankingLevel();
        }
        return null;
    }

    private ArrayList<PointPosition> createNextRankingLevel() {
        inLevelGameNumber++;
        if (inLevelGameNumber >= MAX_LEVEL_FOR_RANKING) {
            currentLevel = 0;
            inLevelGameNumber = 0;
            return null;
        }
        if (inLevelGameNumber == MAX_SMALL_LEVEL_FOR_RANKING) {
            gameSize = MEDIUM;
            currentLevel = 0;
        }
        if (inLevelGameNumber == MAX_MEDIUM_LEVEL_FOR_RANKING) {
            gameSize = BIG;
            currentLevel = 0;
        }
        return LevelBuilder.forceGenerateLevel(gameSize, createLinksBasedOnDifficulty());
    }

    private ArrayList<PointPosition> createNextTrainingLevel() {
        if (currentLevel >= MAX_LEVEL_FOR_TRAINING) {
            currentLevel = 0;
            return null;
        }
        return LevelBuilder.forceGenerateLevel(gameSize, createLinksBasedOnDifficulty());
    }

    private int createLinksBasedOnDifficulty() {
        currentLevel++;
        switch (difficulty) {
            case EASY:
                if (gameSize == SMALL) {
                    return EASY_SMALL[currentLevel - 1];
                }
                if (gameSize == MEDIUM) {
                    return EASY_MEDIUM[currentLevel - 1];
                }
                if (gameSize == BIG) {
                    return EASY_BIG[currentLevel - 1];
                }
            case MEDIUM:
                if (gameSize == SMALL) {
                    return MEDIUM_SMALL[currentLevel - 1];
                }
                if (gameSize == MEDIUM) {
                    return MEDIUM_MEDIUM[currentLevel - 1];
                }
                if (gameSize == BIG) {
                    return MEDIUM_BIG[currentLevel - 1];
                }
            case HARD:
                if (gameSize == SMALL) {
                    return HARD_SMALL[currentLevel - 1];
                }
                if (gameSize == MEDIUM) {
                    return HARD_MEDIUM[currentLevel - 1];
                }
                if (gameSize == BIG) {
                    return HARD_BIG[currentLevel - 1];
                }
        }
        return 0;
    }

    public GameSize getCurrentGameSize() {
        return gameSize;
    }
}
