package achozen.rememberme.engine;

import android.content.Context;

import java.util.ArrayList;
import java.util.Random;

import achozen.rememberme.PointsInitializer;
import achozen.rememberme.enums.Difficulty;
import achozen.rememberme.enums.GameMode;
import achozen.rememberme.enums.GameSize;
import achozen.rememberme.interfaces.GameProgressListener;
import achozen.rememberme.interfaces.PointPosition;
import achozen.rememberme.statistics.GameStatistics;

/**
 * Created by Achozen on 2016-05-28.
 */
public class GameProgressCoordinator {

    private static GameProgressListener gameProgressListener;
    private static LevelDifficultyManager levelDifficultyManager;
    private Context context;
    private GameMode gameMode;
    private String gameId;

    public GameProgressCoordinator(Context context, GameProgressListener progressListener, GameMode gameMode) {
        gameProgressListener = progressListener;
        this.context = context;
        this.gameMode = gameMode;
        gameId = generateRandomGameId();
    }

    public void startGame() {
        if (gameMode == GameMode.RANKING) {
            initiateForRanking();
        } else {
            initiateForTraining();
        }

        startNextLevel(null);
    }

    public void startNextLevel(GameStatistics gameStatistics) {
        GameInitializationData initData2 = prepareNextLevel();
        if (initData2 == null) {
            gameProgressListener.onGameFinished(gameStatistics, gameId);
        } else {
            gameProgressListener.startNewLevel(initData2);
        }
    }

    public String getActiveGameId() {
        return gameId;
    }

    private void initiateForTraining() {
        String difficulty = PeferencesUtil.readFromPrefs(context, PeferencesUtil
                .Preferences.DIFFICULTY);
        String gameSize = PeferencesUtil.readFromPrefs(context, PeferencesUtil
                .Preferences.SIZE);

        levelDifficultyManager = new LevelDifficultyManager(Difficulty.valueOf(difficulty),
                GameSize.valueOf(gameSize));
    }

    private void initiateForRanking() {
        String difficulty = PeferencesUtil.readFromPrefs(context, PeferencesUtil
                .Preferences.DIFFICULTY);

        levelDifficultyManager = new LevelDifficultyManager(Difficulty.valueOf(difficulty));
    }

    private GameInitializationData prepareNextLevel() {
        ArrayList<PointPosition> pattern = levelDifficultyManager.createPatternForNextLevel();
        if (pattern == null) {
            return null;
        }

        return new GameInitializationData(levelDifficultyManager.getCurrentGameSize(),
                PointsInitializer.generateLockPoints(levelDifficultyManager.getCurrentGameSize()), pattern);
    }

    private String generateRandomGameId() {
        Random rand = new Random();
        return "game_" + rand.nextInt(100);
    }
}
