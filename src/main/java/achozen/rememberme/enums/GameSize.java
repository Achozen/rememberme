package achozen.rememberme.enums;

import achozen.rememberme.interfaces.Settings;

/**
 *  Small is 3x3
 *  Medium is 4x4
 *  Big is 5x5
 */
public enum GameSize implements Settings {
    SMALL,
    MEDIUM,
    BIG
}
