package achozen.rememberme.enums;

import achozen.rememberme.interfaces.Settings;

/**
 * Created by Achozen on 2016-05-27.
 */
public enum Difficulty implements Settings {
    EASY ,
    MEDIUM ,
    HARD


}
