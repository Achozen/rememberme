package achozen.rememberme.statistics;

/**
 * Created by Achozen on 2016-06-08.
 */
public enum GameState {
    SUCCESS,
    FAILED
}
